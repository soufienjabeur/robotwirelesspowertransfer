package wrsn;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import io.jbotsim.core.Message;
import io.jbotsim.core.Node;
import io.jbotsim.core.Point;
import io.jbotsim.ui.icons.Icons;

public class BaseStation extends Node {

	// An array of double, in it we will put the sanpshot of every sensor
	Double[] sensorSnapshotRecevied = new Double[2];
	// A hashmap, in it will put the hashMap sensorsSnap sent by the sensor
	Map<Sensor, Double[]> sensorsSnapRecevied = new HashMap<Sensor, Double[]>();
	// A hashmap of robots, while the key is the robot and the value is timing
	Map<Robot, Integer> robots = new HashMap<Robot, Integer>();

	// A hashMap to send to a the chosen robot where the key is the target and the
	// value is the current batery value
	Map<Point, Double> robotTraget = new HashMap<Point, Double>();

	@Override
	public void onStart() {
		setIcon(Icons.STATION);
		setIconSize(16);

		// Initiates tree construction with an empty message
		sendAll(new Message(null, "INIT"));
	}

	@Override
	public void onMessage(Message message) {
		super.onMessage(message);

		if (message.getFlag() == "SENSING" && message.getSender() instanceof Sensor) {

			// Sorting the hashmap sent by the sensor and puting it in the
			// sensorsSnapRecevied
			sensorsSnapRecevied.putAll((Map<Sensor, Double[]>) message.getContent());

			Point target = null;

			// loop ever the hashMap sensorsSnapRecevied
			for (Sensor sensor : sensorsSnapRecevied.keySet()) {

				sensorSnapshotRecevied = sensorsSnapRecevied.get(sensor);

				target = sensor.getLocation();
				// max distance marker
				double maxDistance = 1000;
				// The robot will charge the target sensor

				Robot robotChoice = null;

				for (Robot robot : robots.keySet()) {

					if (maxDistance > robot.distance(sensor)) {
						maxDistance = robot.distance(sensor);
						robotChoice = robot;
					}
				}

				robotTraget.put(target, sensorSnapshotRecevied[1]);
				// Send the target to the chosen robot
				robotTraget.toString();
				send(robotChoice, new Message(robotTraget, "TARGET"));

				// remove the sensor from the hasmap

				robotTraget.remove(sensor);

				sensorsSnapRecevied.remove(sensor);
			}

		}
		if ((message.getFlag() == "ALIVE" || message.getSender() instanceof Robot)
				&& !robots.containsKey(message.getSender())) {
			robots.put((Robot) (message.getSender()), getTime());
		}
	}

}