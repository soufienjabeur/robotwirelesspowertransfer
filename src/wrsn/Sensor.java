package wrsn;

import java.util.HashMap;
import java.util.Map;

import io.jbotsim.core.Color;
import io.jbotsim.core.Message;
import io.jbotsim.core.Node;

public class Sensor extends Node {
	Node parent = null;
	int battery = 255;
	// An array of double, it will contains a snapshot of the sensed value and the
	// battery value of the sensor
	Double[] sensorSnapshot = new Double[2];
	// a hashmap (key, value): where the key is the id of the sensor and the value
	// will be the array sensorSnapshot
	Map<Sensor, Double[]> sensorsSnap = new HashMap<Sensor, Double[]>();
	Color previousColor;
	@Override
	public void onMessage(Message message) {
		// "INIT" flag : construction of the spanning tree
		// "SENSING" flag : transmission of the sensed values
		// You can use other flags for your algorithms
		if (message.getFlag().equals("INIT")) {
			// if not yet in the tree
			if (parent == null) {
				// enter the tree
				parent = message.getSender();
				getCommonLinkWith(parent).setWidth(4);
				// propagate further
				sendAll(message);
			}
		} else if (message.getFlag().equals("SENSING")) {
			// retransmit up the tree
			send(parent, message);
		}
	}

	@Override
	public void send(Node destination, Message message) {
		if (battery > 0) {
			super.send(destination, message);
			battery--;
			updateColor();
		}
	}

	@Override
	public void onClock() {
		if (parent != null) { // if already in the tree
			if (Math.random() < 0.02) { // from time to time...
				double sensedValue = Math.random(); // sense a value

				// Adding the current value to the array
				sensorSnapshot[0] = sensedValue;
				sensorSnapshot[1] = (double) battery;

				// the color of the link between the parent and child depend of the value of
				// battery
				Color c = new Color(255 - battery, 255 - battery, 255);
				getCommonLinkWith(parent).setColor(c);

				// Adding the sensorSnapshot of a sensor to the hashMap
				sensorsSnap.put(this, sensorSnapshot);
				send(parent, new Message(sensorsSnap, "SENSING")); // send it to parent

			}
		}
	}

	protected void updateColor() {
		setColor(battery == 0 ? Color.red : new Color(255 - battery, 255 - battery, 255));
	}
}