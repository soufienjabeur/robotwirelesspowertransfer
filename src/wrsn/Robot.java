package wrsn;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import io.jbotsim.core.Message;
import io.jbotsim.core.Node;
import io.jbotsim.core.Point;
import io.jbotsim.ui.icons.Icons;

public class Robot extends WaypointNode {

	Point target;
	Point parking;
	BaseStation baseStation = null;
	Robot robot = null;
	// A hashMap to receive the hashmap sent by Base station which contains the
	// chosen robot where the key is the target and the value is the current batery
	// value
	Map<Point, Double> robotTraget = new HashMap<Point, Double>();
	Map<Point, Double> aux = new HashMap<Point, Double>();

	static final int threshold = 3;

	@Override
	public void onStart() {
		setSensingRange(30);
		setIcon(Icons.ROBOT);
		setIconSize(16);

		// Initial position of the robots
		target = parking = getLocation();
		onArrival();
	}

	@Override
	public void onSensingIn(Node node) {
		if (node instanceof Sensor)
			((Sensor) node).battery = 255;
	}

	@Override
	public void onArrival() {
		
		addDestination(target.getX(), target.getY());
		destinations.remove();
	}

	@Override
	public void onMessage(Message message) {

		super.onMessage(message);
		// receive the hashmap sent by Base station which contains the
		// chosen robot where the key is the target and the value is the current batery
		// value
		if (message.getSender() instanceof BaseStation) {
			if (message.getFlag().equals("TARGET")) {

				// receive the hashmap sent by Base station
				this.aux = ((Map<Point, Double>) message.getContent());
				// sorts the hashmap according to the battery value
				this.robotTraget = sortByValue(aux);

				this.baseStation = (BaseStation) message.getSender();

				for (Point targetSensor : this.robotTraget.keySet()) {
					// Adding the new received target to the queue destination
					if (!this.destinations.contains(targetSensor) && !robot.destinations.contains(targetSensor)) {
						this.destinations.add(targetSensor);

						// This part is used to balance the load between the two robots
						if (this.destinations.size() - this.robot.destinations.size() >= threshold) {
							for (int i = 0; i < this.destinations.size(); i++) {
								if (i > this.destinations.size() / 2
										&& this.destinations.size() - this.robot.destinations.size() >= threshold) {
									target = this.destinations.remove();
									
									// Send to the other robot
									sendAll(new Message(target, "TFR"));// TFR: Target From Robot

								}
							}
						} else {

							// Update the new current target
							setTarget(this.destinations.peek());
						}

					}

				}

			}
		}
		// If the robot receives a message from another robot
		else if (message.getSender() instanceof Robot && !this.equals((Robot) message.getSender())) {

			if (message.getFlag().equals("ALIVE")) {
				robot = (Robot) message.getSender();
			}

			// If the robot receives sensors to visit from another robot
			else if (message.getFlag().equals("TFR")) {
				Point target = (Point) message.getContent();
				if (!this.destinations.contains(target) && !robot.destinations.contains(target)) {
					this.destinations.add(target);

					// This part is used to balance the load between the two robots
					if (this.destinations.size() - ((Robot) message.getSender()).destinations.size() >= threshold) {
						for (int i = 0; i < this.destinations.size(); i++) {
							if (i > this.destinations.size() / 2
									&& this.destinations.size() - this.robot.destinations.size() >= threshold) {
								target = this.destinations.remove();

								// Send to the other robot
								sendAll(new Message(target, "TFR"));// TFR: Target From Robot
							}
						}
					} else {
						// Update the new current target

						setTarget(this.destinations.peek());
					}
				}

			}
			// If the robot receives the message from another robot
			else if (message.getFlag().equals("SMT")) {// SMT: Send Me Target

				for (int i = 0; i < this.destinations.size(); i++) {
					if (i > this.destinations.size() / 2) {
						Point target = this.destinations.remove();
						sendAll(new Message(target, "TFR"));
					}
				}
			}

		}

	}

	// Update the target every time
	public void setTarget(Point target) {
		this.target = target;

	}

	@Override
	public void onClock() {
		super.onClock();

		if (target == parking) {

			if (baseStation != null) {

				sendAll(new Message(null, "ALIVE"));

			} else {
				sendAll(new Message(null, "ALIVE"));
			}

		}

		// the robot asks the second robot to send it targets
		else if (this.destinations.size() == 0) {
			sendAll(new Message(null, "SMT"));// SMT: Send Me Target
		}

		// We have noticed that robots tend not to visit sensors close to the base
		// station
		else if ((int) (10 * Math.random()) < 7) {

			for (Node n : baseStation.getInNeighbors()) {
				Point point = n.getLocation();
				setTarget(point);
			}

		}
	}

	// function to sort hashMap by values
	public static HashMap<Point, Double> sortByValue(Map<Point, Double> hm) {
		// Create a list from elements of HashMap
		List<Map.Entry<Point, Double>> list = new LinkedList<Map.Entry<Point, Double>>(hm.entrySet());

		// Sort the list
		Collections.sort(list, new Comparator<Map.Entry<Point, Double>>() {
			public int compare(Map.Entry<Point, Double> o1, Map.Entry<Point, Double> o2) {
				return (o2.getValue()).compareTo(o1.getValue());
			}
		});

		// put data from sorted list to hashmap
		HashMap<Point, Double> temp = new LinkedHashMap<Point, Double>();
		for (Map.Entry<Point, Double> aa : list) {
			temp.put(aa.getKey(), aa.getValue());
		}
		return temp;
	}

}